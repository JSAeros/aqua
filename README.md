Proyecto Triple Asignatura - Equipo de Desarollo LIQUIDFLY STUDIOS

Aqua, un videojuego que guiará al jugador a través de los problemas de un mundo fantástico
que cada vez se encuentra más deteriorado por el uso de una magia (que representa la industria)
que sustituye a la magia natural del propio mundo (representando los recursos naturales). Con un
fuerte mensaje de conciencia, este proyecto tiene como intención el concienciar a sus jugadores
a cerciorarse de la proveniencia de los recursos que consumen, a exigir una explotación equilibrada
y correcta de los mismos, y a tener una conciencia de consumo del agua que corresponda 
a los retos que plantean la vida moderna.
