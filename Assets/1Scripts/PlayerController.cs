﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Vector2 target;

    Animator animator;

    public float playerSpeed = 5f;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }
    void Update()
    {

        checkPlayerMovement();

    }

    private void checkPlayerMovement()
    {
        float distance;
        float movementDirection;
        float minimumThreshold = 0.1f;

        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            target = new Vector2(mousePosition.x, transform.position.y);           
        }

        distance = Vector2.Distance(target, transform.position);
        movementDirection = target.x - transform.position.x;


        if (target != Vector2.zero)
        {
            if (distance > minimumThreshold)
            {
                transform.position = Vector2.MoveTowards(transform.position, target, Time.deltaTime * playerSpeed);
                if (movementDirection > 0) animator.SetInteger("runningState", 1);
                else animator.SetInteger("runningState", -1);
            }
            else animator.SetInteger("runningState", 0);
        }
    }
}
